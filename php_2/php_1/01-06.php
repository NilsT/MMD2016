<?php

$a = "Dette er en tekststreng<br>";
echo $a;

$a = 'Dette er en tekststreng<br>';
echo $a;

$name = "Nils";
echo "hej, $name";
echo "<br>";
echo 'hej, ' . $name;

$a = "<p>Dette er en tekststreng der indeholder afsnitselementer</p>";
echo $a;

date_default_timezone_get("Europe/Copenhagen");
$a = "<div>Dato i dag er: " . date("d m y") . " og kl. er: " . date("h:i:s");
echo $a; 

echo "<br>";
echo "Er de to variabler ens?<br>";
$a = "abc";
$b = "abc";

if($a == $b)
{
    echo ' ja, ' . $a . ' og ' . $b . ' er end <br>';
}