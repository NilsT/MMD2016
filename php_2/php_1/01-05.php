<?php

    $a = 1234;
    var_dump($a);

    $a = -123;
    var_dump($a);

    $a = 0123;
    var_dump($a);

    $a = 1.3;
    var_dump($a);

    $large_number = 2147483647;
    var_dump($large_number);

    $large_number = 2147483648;
    var_dump($large_number);

    $million = 10000000;
    $large_number = 50000 * $million;
    var_dump($large_number);

    var_dump(25/7);
    var_dump((int) (25/7));
    var_dump(round(25/7));

?>