<?php
    /*
     * Opgave 03_01
     * 
     * En event er en begivendhed der er afgrænset ved tid og sted, og som er kendetegnet ved specifikt indhold.
     * 
     * Opgave 1) 
     * Find nogle flere events. Søg inspiration på http://www.visitskive.dk/skive/begivenheder-2
     * Indsæt de fundne events i $events arrayet nede i klassen Event. Find latitude og longitude på https://itouchmap.com/latlong.html
     *
     * Opgave 2)
     * I klassen Event er der en tom metode der hedder getAllEvents. Metoden skal returnere hele $events arrayet fra klassen.
     * Når vi returnerer hele arrayet, så kan vi anvende det andre steder. Fx i index.php
     *
     * Opgave 3)
     * Gå til index.php
     * Lokaliser der hvor markører bliver indsat på kortet. Erstat de to eksisterende markører med php kode. Koden skal løbe igennem det array, som metoden getAllEvents 
     * returnerer og udskrive indholdet, således, at hver gang løkken løber en gang, vises en markør på kortet.
     * 
     * 
     */
    
    class Event
    {
        private $events = array(
            array(
            "EventId"=>1,
            "EventName"=>"Rave party",
            "EventDescription"=>"For young people",
            "EventDate"=>"Oktober 1 2016 10:00pm",
            "Lat"=>"56.4",
            "Long"=>"9",
            "EventImage"=>"img/rave.png"
        ),
        array(
            "EventId"=>2,
            "EventName"=>"Opera",
            "EventDescription"=>"For not so young people",
            "EventDate"=>"Oktober 2 2016 10:00pm",
            "Lat"=>"56.3",
            "Long"=>"9.4",
            "EventImage"=>"img/opera.png"
        ),
        array(
            "EventId"=>3,
            "EventName"=>"Metal",
            "EventDescription"=>"For everybody",
            "EventDate"=>"Oktober 2 2016 2:00am",
            "Lat"=>"56.4",
            "Long"=>"9.3",
            "EventImage"=>"img/metal.png"
        ),
        array(
            "EventId"=>4,
            "EventName"=>"Træskibssejlads",
            "EventDescription"=>"Limfjorden Rundt Træskibssejlads",
            "EventDate"=>"13/09-17/09 2016",
            "Lat"=>"56.57",
            "Long"=>"9.05",
            "EventImage"=>"img/ship.png"
        ));
        function __construct()
        {
            //Konstruktør (funktionen) skal ikke benyttes
        }
        function singleEvent($EventId)
        {
            //Denne funktion skal returnere alle event i events arrayet
            foreach($this->events as $ev){

                $keys = array_keys($ev);
                $values = array_values($ev);

                if($ev["EventId"] == $EventId) {
                    echo "L.marker([". $values[4] . ", " . $values[5] . "]).addTo(mymap).bindPopup('<b>" . $values[1] . "</b><br />" . $values[3] . "');";
                }
            }
        }

        function getAllEvents(){
            foreach($this->events as $ev){

                $keys = array_keys($ev);
                $values = array_values($ev);

                echo "L.marker([". $values[4] . ", " . $values[5] . "]).addTo(mymap).bindPopup('<b>" . $values[1] . "</b><br />" . $values[3] . "');";

                
            }
        }
    }
?>