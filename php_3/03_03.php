<?php
    /*
     * Opgave 03_02
     * 
     * Metoden metoden getAllValuesFromAnArray skal kunne finde værdierne i et associativt array.
     * Brug den indbyggede metode array_keys().
     * Se kapitel - Array -> Extracting multiple values og afsnittet Keys and values
     */
    
    class Event
    {
        function getAllValuesFromAnArray()
        {
            $event = array(
            "Event ID"=>3,
            "Event name"=>"Metal",
            "Event description"=>"For everybody",
            "Event date"=>"Oktober 2 2016 2:00am",
            "Lat"=>"56.4",
            "Long"=>"9.3",
            "Event image"=>"img/metal.png"
            );

            $keys = array_keys($event); //her får de forskellige keys (venste spalte) hver en value i arrayet $keys

            $values = array_values($event); //her får de forskellige values (højre spalte) hver en value i arrayet $values

            echo $keys[0] . ": " . $values[0] . ",<br> " . $keys[1] . ": " . $values[1] . ",<br> " . $keys[2] . ": " . $values[2] . ",<br> " . $keys[3] . ": " . $values[3] . ",<br> " . $keys[4] . ": " . $values[4] . ",<br> " . $keys[5] . ": " . $values[5] . ",<br> " . $keys[6] . ": " . $values[6];
        }

    }
    $event = new Event;
    $event->getAllValuesFromAnArray();
?>